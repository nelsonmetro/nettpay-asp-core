﻿using Abp.Authorization;
using nettpay.Authorization.Roles;
using nettpay.Authorization.Users;

namespace nettpay.Authorization
{
    public class PermissionChecker : PermissionChecker<Role, User>
    {
        public PermissionChecker(UserManager userManager)
            : base(userManager)
        {
        }
    }
}
