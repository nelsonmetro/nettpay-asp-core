﻿namespace nettpay
{
    public class nettpayConsts
    {
        public const string LocalizationSourceName = "nettpay";

        public const string ConnectionStringName = "Default";

        public const bool MultiTenancyEnabled = true;
    }
}
