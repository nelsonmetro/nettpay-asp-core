using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Domain.Services;

namespace nettpay.Owners
{
    public interface IOwnerManager : IDomainService
    {
         Task<Owner> GetAsync(long id);

         Task<IReadOnlyList<Owner>> GetListAsync();

        Task CreateAsync(Owner @owner);

        Task DeleteAsync(Owner @owner);
    }
}