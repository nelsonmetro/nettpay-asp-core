using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using Abp.Events.Bus;
using Microsoft.EntityFrameworkCore;

namespace nettpay.Owners
{
    public class OwnerManager : IOwnerManager
    {
        public IEventBus EventBus { get; set; }
        private readonly IRepository<Owner, long> _ownerRepository;
        protected OwnerManager(
            IRepository<Owner, long> ownerRepository
        )
        {
            _ownerRepository = ownerRepository;
            EventBus = NullEventBus.Instance;
        }
        public async Task CreateAsync(Owner owner)
        {
            await _ownerRepository.InsertAsync(@owner);
        }

        public async Task DeleteAsync(Owner owner)
        {
            await _ownerRepository.DeleteAsync(@owner);
        }

        public async Task<Owner> GetAsync(long id)
        {
            var @owner = await _ownerRepository.FirstOrDefaultAsync(id);
            if (@owner == null)
            {
                throw new Abp.UI.UserFriendlyException("Could not found the owner, maybe it's deleted!");
            }
            return @owner;
        }

        public async Task<IReadOnlyList<Owner>> GetListAsync()
        {
            return await _ownerRepository
                .GetAll()
                .ToListAsync();
        }
    }
}