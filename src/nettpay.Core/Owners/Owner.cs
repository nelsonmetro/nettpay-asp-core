using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities;
using nettpay.Authorization.Users;
using nettpay.Businesses;

namespace nettpay.Owners
{
    [Table("owner")]
    public class Owner : Entity<long>
    {
        [ForeignKey("Id")]
        public virtual User User { get; protected set; }

        [Key]
        public override long Id { get; set; }
        public virtual ICollection<Business> Businesses { get; protected set; }
        protected Owner()
        {

        }
        public static Owner Create(long id)
        {
            var @owner = new Owner
            {
                Id = id
            };
            @owner.Businesses = new Collection<Business>();
            return @owner;
        }
        public static Owner Edit()
        {
            var @owner = new Owner
            {

            };
            return @owner;
        }
        public static Owner Delete()
        {
            var @owner = new Owner
            {

            };
            return @owner;
        }
    }
}