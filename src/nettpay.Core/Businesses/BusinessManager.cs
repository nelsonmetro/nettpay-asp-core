using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using Abp.Events.Bus;
using Microsoft.EntityFrameworkCore;
using nettpay.Owners;

namespace nettpay.Businesses
{
    public class BusinessManager : IBusinessManager
    {
        public IEventBus EventBus { get; set; }
        private readonly IRepository<Business, Guid> _businessRepository;
        private readonly IRepository<Owner, long> _ownerRepository;

        public BusinessManager(
             IRepository<Business, Guid> businessRepository,
             IRepository<Owner, long> ownerRepository
        )
        {
            _businessRepository = businessRepository;
            _ownerRepository = ownerRepository;
            EventBus = NullEventBus.Instance;
        }
        public async Task<Business> GetAsync(Guid id)
        {
            var @business = await _businessRepository.GetAsync(id);
            if (@business == null)
            {
                throw new Abp.UI.UserFriendlyException("Could not found the business, maybe it's deleted!");
            }

            return @business;
        }

        public async Task<IReadOnlyList<Business>> GetListByOwnerAsync(Owner owner)
        {
            return await _businessRepository
                .GetAll()
                .Where(business => business.OwnerId == @owner.Id)
                .ToListAsync();
        }

        public async Task CreateAsync(Business business)
        {
            //  First Check if owner record exists
            var @owner = await _ownerRepository.FirstOrDefaultAsync(@business.OwnerId);
            if (@owner == null)
            { // If it doesn't, create one
                @owner = Owner.Create(@business.OwnerId);
                await _ownerRepository.InsertAsync(@owner);
            }
            await _businessRepository.InsertAsync(@business);
        }

        public async Task DeleteAsync(Business business)
        {
            await _businessRepository.DeleteAsync(@business);
            //  First Check if owner still owns any businesses
            var @owner = await _ownerRepository.FirstOrDefaultAsync(@business.OwnerId);
            if (@owner.Businesses.Count < 1)
            {
                await _ownerRepository.DeleteAsync(@owner);
            }
        }
    }
}