using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using nettpay.Employees;
using nettpay.Owners;

namespace nettpay.Businesses {
    [Table("business")]
    public class Business: FullAuditedEntity < Guid > , IMustHaveTenant {
        public
        const int MaxTitleLength = 128;
        public
        const int MaxDescriptionLength = 2048;

        public
        const int MaxAddressLength = 45;
        public
        const int MaxPhoneLength = 13;

        public virtual int TenantId { get;set; }
        [Required]
        public virtual string RegNo { get; protected set; }

        [Required]
        [StringLength(MaxTitleLength)]
        public virtual string Title { get; protected set; }

        [StringLength(MaxDescriptionLength)]
        public virtual string Description { get; protected set; }


        [StringLength(MaxAddressLength)]
        public virtual string AddressLine1 { get; protected set; }
        [StringLength(MaxAddressLength)]
        public virtual string AddressLine2 { get; protected set; }
        [StringLength(MaxAddressLength)]
        public virtual string Suburb { get; protected set; }

        public virtual string ZipCode { get; protected set; }


        [StringLength(MaxPhoneLength)]
        public virtual string Telephone { get; protected set; }

        [StringLength(MaxTitleLength)]
        public virtual string WebURL { get; protected set; }

        public virtual byte ? MonthEnd { get; protected set; }

        public virtual string PAYENo { get; protected set; }
        public virtual string UIFNo { get; protected set; }

        [StringLength(MaxTitleLength)]
        public virtual string BankName { get; set; }
        public virtual string AccountNo { get; set; }
        [StringLength(MaxTitleLength)]
        public virtual string BranchName { get; protected set; }
        public virtual string BranchCode { get; protected set; }

        public virtual string VATNo { get; protected set; }
        public virtual string BEEStatus { get; protected set; }

        [ForeignKey("OwnerId")]
        public virtual Owner Owner { get; protected set; }
        public virtual long OwnerId { get; protected set; }

        public virtual ICollection < Employee > Employees { get;  protected set; }

        /// <summary>
        /// We don't make constructor public and forcing to create events using <see cref="Create"/> method.
        /// But constructor can not be private since it's used by EntityFramework.
        /// Thats why we did it protected.
        /// </summary>
        protected Business() {

        }
        public static Business Create(
            int tenantId,
            string title,
            string web,
            string description,
            string regNo,
            string vatNo,
            string bee,
            byte payDate,
            string payeNo,
            string uifNo,
            string tel,
            long uid
        ) {
            var @business = new Business {
                Id = Guid.NewGuid(),
                    TenantId = tenantId,
                    Title = title,
                    WebURL = web,
                    Description = description,
                    RegNo = regNo,
                    VATNo = vatNo,
                    UIFNo = uifNo,
                    BEEStatus = bee,
                    Telephone = tel,
                    PAYENo = payeNo,
                    MonthEnd = payDate,
                    OwnerId = uid
            };
            @business.Employees = new Collection < Employee > ();

            return @business;
        }
        public void Edit(
            string title,
            string web,
            string description,
            string regNo,
            string vatNo,
            string bee,
            byte payDate,
            string payeNo,
            string uifNo,
            string tel
            ) {
            Title = title;
            WebURL = web;
            Description = description;
            RegNo = regNo;
            VATNo = vatNo;
            UIFNo = uifNo;
            BEEStatus = bee;
            Telephone = tel;
            PAYENo = payeNo;
            MonthEnd = payDate;
        }
        public static Business Delete() {
            var @business = new Business {

            };
            return @business;
        }


        public void EditBankning(
            string title,
            string accno,
            string branchCode,
            string branchName
        ) {
            BankName = title;
            AccountNo = accno;
            BranchCode = branchCode;
            BranchName = branchName;
        }

        public void EditAddress(
            string addr1,
            string addr2,
            string suburb,
            string zip
        ) {
            AddressLine1 = addr1;
            AddressLine2 = addr2;
            Suburb = suburb;
            ZipCode = zip;
        }
    }
}