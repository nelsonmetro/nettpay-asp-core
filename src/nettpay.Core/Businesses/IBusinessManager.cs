using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Domain.Services;
using nettpay.Owners;

namespace nettpay.Businesses
{
    public interface IBusinessManager : IDomainService
    {
        Task<Business> GetAsync(Guid id);
        Task<IReadOnlyList<Business>> GetListByOwnerAsync(Owner @owner);

        Task CreateAsync(Business @business);

        Task DeleteAsync(Business @business);
    }
}