using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities;
using nettpay.Authorization.Users;
using nettpay.Businesses;

namespace nettpay.Employees
{
    [Table("employee")]
    public class Employee : Entity<long>
    {
        public const int MaxIdNoLength = 13;
        public const int MaxNameLength = 20;
        public const int MaxJobTitleLength = 35;
        public const int MaxAddressLength = 45;

        [ForeignKey("Id")]
        public virtual User User { get; protected set; }

        [Key]
        public override long Id { get; set; }

        [StringLength(MaxIdNoLength)]
        public virtual string IdNo { get; protected set; }
        [StringLength(MaxNameLength)]
        public virtual string KnownAs { get; protected set; }

        [Required]
        [StringLength(MaxJobTitleLength)]
        public virtual string JobTilte { get; protected set; }
        public virtual DateTime DateEngaged { get; protected set; }
        public virtual string TaxRefNo { get; protected set; }

        [StringLength(MaxAddressLength)]
        public virtual string AddressLine1 { get; protected set; }
        [StringLength(MaxAddressLength)]
        public virtual string AddressLine2 { get; protected set; }
        [StringLength(MaxAddressLength)]
        public virtual string Suburb { get; protected set; }

        public virtual string ZipCode { get; protected set; }

        [ForeignKey("EmpTypeId")]
        public virtual EmploymentType EmploymentType { get; protected set; }
        public virtual int EmpTypeId { set; get; }


        [ForeignKey("BusinessId")]
        public virtual Business Business { get; protected set; }
        public virtual Guid BusinessId { get; protected set; }

        protected Employee()
        {

        }

        public static Employee Create(
           long userId,
           string idNo,
           string knownAs,
           Guid businessId
       )
        {
            var @employee = new Employee
            {
                Id = userId,
                IdNo = idNo,
                KnownAs = knownAs,
                BusinessId = businessId
            };
            return @employee;
        }
        public static Employee Edit(
            long id,
            string idNo,
            string knownAs,
            Guid businessId
        )
        {
            var @employee = new Employee
            {
                Id = id,
                IdNo = idNo,
                KnownAs = knownAs,
                BusinessId = businessId
            };
            return @employee;
        }
        public static Employee Delete()
        {
            var @employee = new Employee
            {

            };
            return @employee;
        }

        public static Employee EditAddress(
            long id,
            string addr1,
            string addr2,
            string suburb,
            string zip
        )
        {
            var @employee = new Employee
            {
                Id = id,
                AddressLine1 = addr1,
                AddressLine2 = addr2,
                Suburb = suburb,
                ZipCode = zip
            };
            return @employee;
        }
        public static Employee EditEmployment(
            long id,
            string title,
            DateTime engaged,
            int empTypeId,
            string payTypeId
        )
        {
            var @employee = new Employee
            {
                Id = id,
                JobTilte = title,
                DateEngaged = engaged,
                EmpTypeId = empTypeId

            };
            return @employee;
        }
        public static Employee EditTax(
            long id,
            string taxref
        )
        {
            var @employee = new Employee
            {
                Id = id,
                TaxRefNo = taxref
            };
            return @employee;
        }
    }
}