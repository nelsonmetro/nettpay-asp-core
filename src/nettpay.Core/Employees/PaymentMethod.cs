using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Threading.Tasks;
using Abp.Domain.Entities;

namespace nettpay.Employees
{
    [Table("paymethod")]
    public class PaymentMethod : Entity<long>
    {
        [Required]

        [ForeignKey("EmployeeId")]
        public virtual Employee Employee { get; protected set; }
        public virtual long EmployeeId { get; protected set; }

        [Required]

        [ForeignKey("MethodTypeId")]
        public virtual PaymentMethodType PaymentMethodType { get; protected set; }
        public virtual string MethodTypeId { get; protected set; }

        protected PaymentMethod()
        {
            
        }
        // public static async Task<PaymentMethod> CreateAsync(Event @event, Employee employee, PaymentMethodType registrationPolicy)
        // {
        //     await registrationPolicy.CheckRegistrationAttemptAsync(@event, user);

        //     return new PaymentMethod
        //     {
        //         TenantId = @event.TenantId,
        //         EventId = @event.Id,
        //         Event = @event,
        //         UserId = @user.Id,
        //         User = user
        //     };
        // }
    }
}