using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using Abp.Events.Bus;
using Abp.UI;
using Microsoft.EntityFrameworkCore;
using nettpay.Businesses;

namespace nettpay.Employees
{
    public class EmployeeManager : IEmployeeManager
    {
        public IEventBus EventBus { get; set; }
        private readonly IRepository<Business, Guid> _businessRepository;
        private readonly IRepository<Employee, long> _employeeRepository;

        public EmployeeManager(
             IRepository<Business, Guid> businessRepository,
             IRepository<Employee, long> employeeRepository
        )
        {
            _businessRepository = businessRepository;
            _employeeRepository = employeeRepository;
            EventBus = NullEventBus.Instance;
        }

        public async Task CreateAsync(Employee employee)
        {
            //  First Check if business record exist
            var @business = await _businessRepository.FirstOrDefaultAsync(@employee.BusinessId);
            if (@business == null)
            {
                throw new UserFriendlyException("Business not found, maybe it's deleted!");
            }
            await _employeeRepository.InsertAsync(@employee);
        }

        public Task DeleteAsync(Employee employee)
        {
            throw new System.NotImplementedException();
        }

        public async Task EditAddressAsync(Employee employee)
        {
            await _employeeRepository.UpdateAsync(@employee);
        }

        public async Task EditAsync(Employee employee)
        {
            await _employeeRepository.UpdateAsync(@employee);
        }

        public async Task EdiTaxAsync(Employee employee)
        {
            await _employeeRepository.UpdateAsync(@employee);
        }

        public async Task EditEmploymentAsync(Employee employee)
        {
            await _employeeRepository.UpdateAsync(@employee);
        }

        public async Task<Employee> GetAsync(long id)
        {
            var @employee = await _employeeRepository.FirstOrDefaultAsync(id);
            if (@employee == null)
            {
                throw new UserFriendlyException("Could not found the employee, maybe it's deleted!");
            }

            return @employee;
        }

        public async Task<IReadOnlyList<Employee>> GetListByBusinessAsync(Business business)
        {
            return await _employeeRepository
                .GetAll()
                .Where(emp => emp.BusinessId == @business.Id)
                .ToListAsync();
        }
    }
}