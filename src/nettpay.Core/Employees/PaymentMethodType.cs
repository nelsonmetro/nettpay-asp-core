using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace nettpay.Employees
{
    [Table("paymethod_type")]
    public class PaymentMethodType
    {
        public const int MaxTitleLength = 45;

        [Key]
        public virtual string Id {get; set;}
        [Required]
        // [Index(IsUnique = true)]
        [StringLength(MaxTitleLength)]
        public virtual string Title {get; set;}
        public PaymentMethodType(string id, string title){
            this.Id = id;
            this.Title = title;
        }
    }
}