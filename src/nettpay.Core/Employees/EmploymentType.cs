using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities;

namespace nettpay.Employees
{
    [Table ("employtype")]
    public class EmploymentType : Entity<int>
    {
        public const int MaxTitleLength = 15;
        private static byte count = 0;
        [Required]
        [StringLength(MaxTitleLength)]
        public virtual string Title {get; set;}
        public EmploymentType(string title){
            // Id = ++count;
            this.Title = title;
        }
    }
}