using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Domain.Services;
using nettpay.Businesses;

namespace nettpay.Employees
{
    public interface IEmployeeManager : IDomainService
    {
        Task<Employee> GetAsync(long id);
        Task<IReadOnlyList<Employee>> GetListByBusinessAsync(Business @business);

        Task CreateAsync(Employee @employee);
        Task EditAsync(Employee @employee);
        Task EditAddressAsync(Employee @employee);
        Task EditEmploymentAsync(Employee @employee);
        Task EdiTaxAsync(Employee @employee);
        Task DeleteAsync(Employee @employee);
    }
}