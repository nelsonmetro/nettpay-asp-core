using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace nettpay.Employees
{
    [Table("banking")]
    public class Banking
    {
        [ForeignKey ("Id")]
        public virtual PaymentMethod PaymentMethod { get; protected set; }

        [Key]
        public virtual long Id { get; protected set; }
        public virtual string MethodType { get; }
        public virtual string BankName { get; protected set; }
        public virtual string AccountNo { get; protected set; }
        public virtual string BranchName { get; protected set; }
        public virtual string BranchCode { get; protected set; }

        protected Banking(){
            this.MethodType = "EFT";
        }
    }
}