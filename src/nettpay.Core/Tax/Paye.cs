
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities;

namespace nettpay.Tax
{
    [Table("PAYE")]
    public class Paye : Entity<int>
    {
         [Required]
         public virtual double Annual { get; protected set; }
         [Required]
         public virtual double Rate { get; protected set; }

         protected Paye()
         {
             
         }
        
    }
}