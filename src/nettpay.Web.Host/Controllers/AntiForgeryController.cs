using Microsoft.AspNetCore.Antiforgery;
using nettpay.Controllers;

namespace nettpay.Web.Host.Controllers
{
    public class AntiForgeryController : nettpayControllerBase
    {
        private readonly IAntiforgery _antiforgery;

        public AntiForgeryController(IAntiforgery antiforgery)
        {
            _antiforgery = antiforgery;
        }

        public void GetToken()
        {
            _antiforgery.SetCookieTokenAndHeader(HttpContext);
        }
    }
}
