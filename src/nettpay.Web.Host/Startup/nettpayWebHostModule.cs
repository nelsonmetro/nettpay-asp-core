﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Abp.Modules;
using Abp.Reflection.Extensions;
using nettpay.Configuration;

namespace nettpay.Web.Host.Startup
{
    [DependsOn(
       typeof(nettpayWebCoreModule))]
    public class nettpayWebHostModule: AbpModule
    {
        private readonly IHostingEnvironment _env;
        private readonly IConfigurationRoot _appConfiguration;

        public nettpayWebHostModule(IHostingEnvironment env)
        {
            _env = env;
            _appConfiguration = env.GetAppConfiguration();
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(nettpayWebHostModule).GetAssembly());
        }
    }
}
