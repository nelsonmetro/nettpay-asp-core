using Abp.AspNetCore.Mvc.Controllers;
using Abp.IdentityFramework;
using Microsoft.AspNetCore.Identity;

namespace nettpay.Controllers
{
    public abstract class nettpayControllerBase: AbpController
    {
        protected nettpayControllerBase()
        {
            LocalizationSourceName = nettpayConsts.LocalizationSourceName;
        }

        protected void CheckErrors(IdentityResult identityResult)
        {
            identityResult.CheckErrors(LocalizationManager);
        }
    }
}
