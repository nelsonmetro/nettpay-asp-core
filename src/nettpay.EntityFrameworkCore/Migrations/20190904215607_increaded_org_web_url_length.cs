﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace nettpay.Migrations
{
    public partial class increaded_org_web_url_length : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "WebURL",
                table: "business",
                maxLength: 128,
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 13,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "UIFNo",
                table: "business",
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 13,
                oldNullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "WebURL",
                table: "business",
                maxLength: 13,
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 128,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "UIFNo",
                table: "business",
                maxLength: 13,
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);
        }
    }
}
