﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace nettpay.Migrations
{
    public partial class domain_layer_it1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "employtype",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Title = table.Column<string>(maxLength: 15, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_employtype", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "owner",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_owner", x => x.Id);
                    table.ForeignKey(
                        name: "FK_owner_AbpUsers_Id",
                        column: x => x.Id,
                        principalTable: "AbpUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "paymethod_type",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    Title = table.Column<string>(maxLength: 45, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_paymethod_type", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "business",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    TenantId = table.Column<int>(nullable: false),
                    RegNo = table.Column<string>(nullable: false),
                    Title = table.Column<string>(maxLength: 128, nullable: false),
                    Description = table.Column<string>(maxLength: 2048, nullable: true),
                    AddressLine1 = table.Column<string>(maxLength: 45, nullable: true),
                    AddressLine2 = table.Column<string>(maxLength: 45, nullable: true),
                    Suburb = table.Column<string>(maxLength: 45, nullable: true),
                    ZipCode = table.Column<string>(nullable: true),
                    Telephone = table.Column<string>(maxLength: 13, nullable: true),
                    WebURL = table.Column<string>(maxLength: 13, nullable: true),
                    PayDate = table.Column<DateTime>(nullable: true),
                    PAYENo = table.Column<string>(nullable: true),
                    UIFNo = table.Column<string>(maxLength: 13, nullable: true),
                    BankName = table.Column<string>(maxLength: 128, nullable: true),
                    AccountNo = table.Column<string>(nullable: true),
                    BranchName = table.Column<string>(maxLength: 128, nullable: true),
                    BranchCode = table.Column<string>(nullable: true),
                    VATNo = table.Column<string>(nullable: true),
                    BEEStatus = table.Column<string>(nullable: true),
                    OwnerId = table.Column<long>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_business", x => x.Id);
                    table.ForeignKey(
                        name: "FK_business_owner_OwnerId",
                        column: x => x.OwnerId,
                        principalTable: "owner",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "employee",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false),
                    IdNo = table.Column<string>(maxLength: 13, nullable: true),
                    KnownAs = table.Column<string>(maxLength: 20, nullable: true),
                    JobTilte = table.Column<string>(maxLength: 35, nullable: false),
                    DateEngaged = table.Column<DateTime>(nullable: false),
                    TaxRefNo = table.Column<string>(nullable: true),
                    AddressLine1 = table.Column<string>(maxLength: 45, nullable: true),
                    AddressLine2 = table.Column<string>(maxLength: 45, nullable: true),
                    Suburb = table.Column<string>(maxLength: 45, nullable: true),
                    ZipCode = table.Column<string>(nullable: true),
                    EmpTypeId = table.Column<int>(nullable: false),
                    BusinessId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_employee", x => x.Id);
                    table.ForeignKey(
                        name: "FK_employee_business_BusinessId",
                        column: x => x.BusinessId,
                        principalTable: "business",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_employee_employtype_EmpTypeId",
                        column: x => x.EmpTypeId,
                        principalTable: "employtype",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_employee_AbpUsers_Id",
                        column: x => x.Id,
                        principalTable: "AbpUsers",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "paymethod",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    EmployeeId = table.Column<long>(nullable: false),
                    MethodTypeId = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_paymethod", x => x.Id);
                    table.ForeignKey(
                        name: "FK_paymethod_employee_EmployeeId",
                        column: x => x.EmployeeId,
                        principalTable: "employee",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_paymethod_paymethod_type_MethodTypeId",
                        column: x => x.MethodTypeId,
                        principalTable: "paymethod_type",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_business_OwnerId",
                table: "business",
                column: "OwnerId");

            migrationBuilder.CreateIndex(
                name: "IX_employee_BusinessId",
                table: "employee",
                column: "BusinessId");

            migrationBuilder.CreateIndex(
                name: "IX_employee_EmpTypeId",
                table: "employee",
                column: "EmpTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_paymethod_EmployeeId",
                table: "paymethod",
                column: "EmployeeId");

            migrationBuilder.CreateIndex(
                name: "IX_paymethod_MethodTypeId",
                table: "paymethod",
                column: "MethodTypeId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "paymethod");

            migrationBuilder.DropTable(
                name: "employee");

            migrationBuilder.DropTable(
                name: "paymethod_type");

            migrationBuilder.DropTable(
                name: "business");

            migrationBuilder.DropTable(
                name: "employtype");

            migrationBuilder.DropTable(
                name: "owner");
        }
    }
}
