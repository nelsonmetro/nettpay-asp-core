﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace nettpay.Migrations
{
    public partial class added_PAYE : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "PayDate",
                table: "business");

            migrationBuilder.AddColumn<byte>(
                name: "MonthEnd",
                table: "business",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "PAYE",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Annual = table.Column<double>(nullable: false),
                    Rate = table.Column<double>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PAYE", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "PAYE");

            migrationBuilder.DropColumn(
                name: "MonthEnd",
                table: "business");

            migrationBuilder.AddColumn<DateTime>(
                name: "PayDate",
                table: "business",
                nullable: true);
        }
    }
}
