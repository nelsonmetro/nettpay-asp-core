using System.Data.Common;
using Microsoft.EntityFrameworkCore;

namespace nettpay.EntityFrameworkCore
{
    public static class nettpayDbContextConfigurer
    {
        public static void Configure(DbContextOptionsBuilder<nettpayDbContext> builder, string connectionString)
        {
            builder.UseSqlServer(connectionString);
        }

        public static void Configure(DbContextOptionsBuilder<nettpayDbContext> builder, DbConnection connection)
        {
            builder.UseSqlServer(connection);
        }
    }
}
