﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using nettpay.Configuration;
using nettpay.Web;

namespace nettpay.EntityFrameworkCore
{
    /* This class is needed to run "dotnet ef ..." commands from command line on development. Not used anywhere else */
    public class nettpayDbContextFactory : IDesignTimeDbContextFactory<nettpayDbContext>
    {
        public nettpayDbContext CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<nettpayDbContext>();
            var configuration = AppConfigurations.Get(WebContentDirectoryFinder.CalculateContentRootFolder());

            nettpayDbContextConfigurer.Configure(builder, configuration.GetConnectionString(nettpayConsts.ConnectionStringName));

            return new nettpayDbContext(builder.Options);
        }
    }
}
