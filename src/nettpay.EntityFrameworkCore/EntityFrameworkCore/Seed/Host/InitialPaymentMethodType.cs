using System.Linq;
using nettpay.Employees;

namespace nettpay.EntityFrameworkCore.Seed.Host
{
    public class InitialPaymentMethodType
    {
        private readonly nettpayDbContext _context;
        public InitialPaymentMethodType(nettpayDbContext context)
        {
            _context = context;
        }
        public void Create(){
            CreateMethodTypes();
        }
        private void CreateMethodTypes(){
            var mTypes = _context.PaymentMethodTypes.FirstOrDefault(x => x.Id == "EFT");
            if(mTypes == null){
                _context.PaymentMethodTypes.Add(new PaymentMethodType("EFT","Electronic Funds Transfer"));
                _context.SaveChanges();
            }
        }
    }
}