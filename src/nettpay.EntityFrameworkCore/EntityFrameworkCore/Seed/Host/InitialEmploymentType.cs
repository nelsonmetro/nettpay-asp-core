using System.Linq;
using nettpay.Employees;

namespace nettpay.EntityFrameworkCore.Seed.Host
{
    public class InitialEmploymentType
    {
        private readonly nettpayDbContext _context;
        public InitialEmploymentType(nettpayDbContext context)
        {
            _context = context;
        }
        public void Create()
        {
            CreateEmploymentTypes();
        }
        private void CreateEmploymentTypes()
        {

            var eTypes = _context.EmploymentTypes.FirstOrDefault(x => x.Title == "Permanent");
            if (eTypes == null)
            {
                _context.EmploymentTypes.Add(new EmploymentType("Contract"));
                _context.EmploymentTypes.Add(new EmploymentType("Permanent"));
                _context.EmploymentTypes.Add(new EmploymentType("Temp"));
                _context.EmploymentTypes.Add(new EmploymentType("Trainee"));

                _context.SaveChanges();
            }
        }
    }
}