﻿namespace nettpay.EntityFrameworkCore.Seed.Host
{
    public class InitialHostDbBuilder
    {
        private readonly nettpayDbContext _context;

        public InitialHostDbBuilder(nettpayDbContext context)
        {
            _context = context;
        }

        public void Create()
        {
            new DefaultEditionCreator(_context).Create();
            new DefaultLanguagesCreator(_context).Create();
            new HostRoleAndUserCreator(_context).Create();
            new DefaultSettingsCreator(_context).Create();
            new InitialPaymentMethodType(_context).Create();
            new InitialEmploymentType(_context).Create();

            _context.SaveChanges();
        }
    }
}
