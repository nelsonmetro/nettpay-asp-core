﻿using Microsoft.EntityFrameworkCore;
using Abp.Zero.EntityFrameworkCore;
using nettpay.Authorization.Roles;
using nettpay.Authorization.Users;
using nettpay.MultiTenancy;
using nettpay.Owners;
using nettpay.Businesses;
using nettpay.Employees;
using nettpay.Tax;

namespace nettpay.EntityFrameworkCore
{
    public class nettpayDbContext : AbpZeroDbContext<Tenant, Role, User, nettpayDbContext>
    {
        /* Define a DbSet for each entity of the application */
        public virtual DbSet<Owner> Owners { get; set; }
        public virtual DbSet<Business> Businesses { get; set; }
        public virtual DbSet<Employee> Employees { get; set; }
        public virtual DbSet<EmploymentType> EmploymentTypes { get; set; }
        public virtual DbSet<PaymentMethod> PaymentMethods { get; set; }
        public virtual DbSet<PaymentMethodType> PaymentMethodTypes { get; set;}
        public virtual DbSet<Paye> TaxTable { get; set;}
        public nettpayDbContext(DbContextOptions<nettpayDbContext> options)
            : base(options)
        {
        }
    }
}
