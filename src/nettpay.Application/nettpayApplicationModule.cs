﻿using Abp.AutoMapper;
using Abp.Modules;
using Abp.Reflection.Extensions;
using nettpay.Authorization;

namespace nettpay
{
    [DependsOn(
        typeof(nettpayCoreModule), 
        typeof(AbpAutoMapperModule))]
    public class nettpayApplicationModule : AbpModule
    {
        public override void PreInitialize()
        {
            Configuration.Authorization.Providers.Add<nettpayAuthorizationProvider>();
             //Adding custom AutoMapper configuration
           Configuration.Modules.AbpAutoMapper().Configurators.Add(CustomDtoMapper.CreateMappings);
        }

        public override void Initialize()
        {
            var thisAssembly = typeof(nettpayApplicationModule).GetAssembly();

            IocManager.RegisterAssemblyByConvention(thisAssembly);

            Configuration.Modules.AbpAutoMapper().Configurators.Add(
                // Scan the assembly for classes which inherit from AutoMapper.Profile
                cfg => cfg.AddProfiles(thisAssembly)
            );
        }
    }
}
