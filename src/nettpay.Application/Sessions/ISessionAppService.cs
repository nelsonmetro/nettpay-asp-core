﻿using System.Threading.Tasks;
using Abp.Application.Services;
using nettpay.Sessions.Dto;

namespace nettpay.Sessions
{
    public interface ISessionAppService : IApplicationService
    {
        Task<GetCurrentLoginInformationsOutput> GetCurrentLoginInformations();
    }
}
