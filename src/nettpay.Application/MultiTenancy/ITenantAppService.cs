﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using nettpay.MultiTenancy.Dto;

namespace nettpay.MultiTenancy
{
    public interface ITenantAppService : IAsyncCrudAppService<TenantDto, int, PagedTenantResultRequestDto, CreateTenantDto, TenantDto>
    {
    }
}

