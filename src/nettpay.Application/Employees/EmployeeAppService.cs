using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using nettpay.Application.Dtos;
using nettpay.Application.Employees;
using nettpay.Authorization.Users;
using nettpay.Businesses.Dtos;
using nettpay.Dtos;

namespace nettpay.Employees
{
    [AbpAuthorize]
    public class EmployeeAppService : nettpayAppServiceBase, IEmployeeAppService
    {
        private readonly IEmployeeManager _empManager;
        private readonly IRepository<Employee, long> _empRepository;
        private readonly UserRegistrationManager _userRegistrationManager;

        public EmployeeAppService(IEmployeeManager empManager, IRepository<Employee, long> empRepository,UserRegistrationManager userRegistrationManager)
        {
            _empManager = empManager;
            _empRepository = empRepository;
            _userRegistrationManager = userRegistrationManager;
        }
        // public async Task CreateAsync(DtoEmployeeInput input)
        // {
        //     //  first register new account
        //     var acc = new RegisterInput{
        //         Name = input.Name,
        //         Surname = input.Surname,
        //         EmailAddress = input.EmailAddress,
        //         UserName = input.UserName,
        //         Password = "123qwe"
        //     };
        //     await new AccountAppService(_userRegistrationManager).Register(acc);
        //     var @emp = Employee.Create(
        //         input.UserId,
        //         input.IdNo,
        //         input.KnownAs,
        //         input.JobTilte,
        //         input.DateEngaged,
        //         input.TaxRefNo,
        //         input.AddressLine1,
        //         input.AddressLine2,
        //         input.Suburb,
        //         input.ZipCode,
        //         input.PayMethodId,
        //         input.BusinessId
        //     );
        //     await _empManager.CreateAsync(@emp);
        // }

        public Task<EmployeeDetailOutput> GetDetailAsync(EntityDto<long> input)
        {
            throw new System.NotImplementedException();
        }

        public Task<ListResultDto<EmployeeListDto>> GetListAsync(BusinessDetailOutputDto input)
        {
            throw new System.NotImplementedException();
        }

        public Task UpdateAsync(DtoEmployeeInput input)
        {
            throw new System.NotImplementedException();
        }
    }
}