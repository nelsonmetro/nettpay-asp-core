using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using nettpay.Application.Dtos;
using nettpay.Businesses.Dtos;
using nettpay.Dtos;

namespace nettpay.Application.Employees
{
    public interface IEmployeeAppService : IApplicationService
    {
        Task<ListResultDto<EmployeeListDto>> GetListAsync(BusinessDetailOutputDto input);
        Task<EmployeeDetailOutput> GetDetailAsync(EntityDto<long> input);
        // Task CreateAsync(DtoEmployeeInput input);
        Task UpdateAsync(DtoEmployeeInput input);
    }
}