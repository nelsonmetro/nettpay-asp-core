using System;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using nettpay.Authorization.Users;
using nettpay.Businesses;
using nettpay.Employees;

namespace nettpay.Dtos
{
    [AutoMapFrom(typeof(Employee))]
    public class EmployeeDetailOutput : EntityDto<long>
    {
        public  User User { get;  set; }
        public  string IdNo { get;  set; }
        public  string KnownAs { get;  set; }
        public  string JobTilte { get;  set; }

        public  DateTime DateEngaged { get;  set; }
        public  string TaxRefNo { get;  set; }
        public  string AddressLine1 { get;  set; }
        public  string AddressLine2 { get;  set; }
        public  string AddressLine3 { get;  set; }
        public  string Cellphone { get;  set; }
        public  PaymentMethod PayMethod { get;  set; }
        public  Business Business { get; set; }
    }
}