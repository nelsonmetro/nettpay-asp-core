using System;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;

namespace nettpay.Employees.Dtos
{
    [AutoMapFrom(typeof(Employee))]
    public class EmployeeDto  : EntityDto<long>
    {
        public virtual long UserId { get; protected set; }
        public virtual string IdNo { get; protected set; }
        public virtual string KnownAs { get; protected set; }
        public virtual string JobTilte { get; protected set; }
        public virtual DateTime DateEngaged { get; protected set; }
        public virtual string TaxRefNo { get; protected set; }

        public virtual string AddressLine1 { get; protected set; }
        public virtual string AddressLine2 { get; protected set; }
        public virtual string Suburb { get; protected set; }
        public virtual string ZipCode { get; protected set; }
        public virtual string Cellphone { get; protected set; }
        public virtual byte PayMethodId { get; protected set; }
        
        public virtual int PackageId { get; protected set; }

        public virtual Guid BusinessId { get; protected set; }
    }
}