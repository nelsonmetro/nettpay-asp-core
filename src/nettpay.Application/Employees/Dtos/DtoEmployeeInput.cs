using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Abp.Auditing;
using Abp.Authorization.Users;
using Abp.Extensions;
using nettpay.Employees;
using nettpay.Validation;

namespace nettpay.Application.Dtos
{
    public class DtoEmployeeInput : IValidatableObject
    {
        public long Id { get; set; }

        [StringLength(AbpUserBase.MaxNameLength)]
        public string Name { get; set; }
        
        [StringLength(AbpUserBase.MaxSurnameLength)]
        public string Surname { get; set; }
        
        [StringLength(AbpUserBase.MaxUserNameLength)]
        public string UserName { get; set; }
        
        [EmailAddress]
        [StringLength(AbpUserBase.MaxEmailAddressLength)]
        public string EmailAddress { get; set; }
        
        [StringLength(AbpUserBase.MaxPlainPasswordLength)]
        [DisableAuditing]
        public string Password { get; set; }
        public long UserId { get; set; }

        [StringLength(Employee.MaxIdNoLength)]
        public string IdNo { get; set; }
        [StringLength(Employee.MaxNameLength)]
        public string KnownAs { get; set; }
        
        [StringLength(Employee.MaxJobTitleLength)]
        public string JobTilte { get; set; }
        public DateTime DateEngaged { get; set; }
        public string TaxRefNo { get; set; }
        [StringLength(Employee.MaxAddressLength)]
        public string AddressLine1 { get; set; }
        [StringLength(Employee.MaxAddressLength)]
        public string AddressLine2 { get; set; }
        [StringLength(Employee.MaxAddressLength)]
        public string Suburb { get; set; }
        public string ZipCode { get; set; }
        public byte PayMethodId { get; set; }
        public Guid BusinessId { get; set; }
        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (!UserName.IsNullOrEmpty())
            {
                if (!UserName.Equals(EmailAddress) && ValidationHelper.IsEmail(UserName))
                {
                    yield return new ValidationResult("Username cannot be an email address unless it's the same as your email address!");
                }
            }
        }
    }
}