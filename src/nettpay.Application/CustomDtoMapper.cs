using AutoMapper;
using nettpay.Application.Dtos;
using nettpay.Businesses;
using nettpay.Businesses.Dtos;

namespace nettpay
{
    internal static class CustomDtoMapper
    {
        public static void CreateMappings(IMapperConfigurationExpression configuration)
       {
           //Business
           configuration.CreateMap<Business, BusinessDetailsInputDto>();
           configuration.CreateMap<Business, BusinessBankingInputDto>();
           configuration.CreateMap<Business, BusinessAddressInputDto>();
       }
    }
}