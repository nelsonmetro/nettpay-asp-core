﻿namespace nettpay.Roles.Dto
{
    public class GetRolesInput
    {
        public string Permission { get; set; }
    }
}
