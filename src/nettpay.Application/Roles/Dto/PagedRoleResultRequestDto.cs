﻿using Abp.Application.Services.Dto;

namespace nettpay.Roles.Dto
{
    public class PagedRoleResultRequestDto : PagedResultRequestDto
    {
        public string Keyword { get; set; }
    }
}

