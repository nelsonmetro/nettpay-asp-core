﻿using System.Threading.Tasks;
using Abp.Application.Services;
using nettpay.Authorization.Accounts.Dto;

namespace nettpay.Authorization.Accounts
{
    public interface IAccountAppService : IApplicationService
    {
        Task<IsTenantAvailableOutput> IsTenantAvailable(IsTenantAvailableInput input);

        Task<RegisterOutput> Register(RegisterInput input);
    }
}
