using System;
using System.Linq;
using System.Security.Claims;

namespace nettpay.Application.Authorization
{
    public static class Token
    {
        public static string SubjectId(this ClaimsPrincipal user) 
        {
            return user?.Claims?.FirstOrDefault(c => c.Type.Equals(ClaimTypes.NameIdentifier, StringComparison.OrdinalIgnoreCase))?.Value;
        }
    }
}