﻿using System.Threading.Tasks;
using nettpay.Configuration.Dto;

namespace nettpay.Configuration
{
    public interface IConfigurationAppService
    {
        Task ChangeUiTheme(ChangeUiThemeInput input);
    }
}
