﻿using System.Threading.Tasks;
using Abp.Authorization;
using Abp.Runtime.Session;
using nettpay.Configuration.Dto;

namespace nettpay.Configuration
{
    [AbpAuthorize]
    public class ConfigurationAppService : nettpayAppServiceBase, IConfigurationAppService
    {
        public async Task ChangeUiTheme(ChangeUiThemeInput input)
        {
            await SettingManager.ChangeSettingForUserAsync(AbpSession.ToUserIdentifier(), AppSettingNames.UiTheme, input.Theme);
        }
    }
}
