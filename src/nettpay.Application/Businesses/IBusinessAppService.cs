using System;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using nettpay.Application.Dtos;
using nettpay.Businesses.Dtos;

namespace nettpay.Businesses
{
    public interface IBusinessAppService : IApplicationService
    {
        Task<ListResultDto<BusinessListDto>> GetListAsync(EntityDto<long> input);
        Task<BusinessDetailOutputDto> GetDetailAsync(EntityDto<Guid> input);
        Task CreateAsync(BusinessDetailsInputDto input);
        Task EditAddressAsync(BusinessAddressInputDto input);
        Task EditBankingAsync(BusinessBankingInputDto input);
        Task UpdateAsync(BusinessInputDto input);
        Task DeleteAsync(EntityDto<Guid> input);
    }
}