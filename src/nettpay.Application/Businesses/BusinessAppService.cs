using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.UI;
using Microsoft.EntityFrameworkCore;
using nettpay.Businesses.Dtos;

namespace nettpay.Businesses
{
    // [AbpAuthorize]
    public class BusinessAppService : nettpayAppServiceBase, IBusinessAppService
    {
        private readonly IBusinessManager _businessManager;
        // private readonly TenantManager _tenantManager;
        private readonly IRepository<Business, Guid> _businessRepository;
        // private readonly IRepository<Tenant, int> _tenantRepo;

        // private readonly SessionAppService _session;
        public BusinessAppService(
            IBusinessManager businessManager
            ,IRepository<Business, Guid> businessRepository
            // ,TenantManager tenantManager
            // ,IRepository<Tenant, int> tenantRepo
            // ,SessionAppService session
            )
        {
            _businessManager = businessManager;
            _businessRepository = businessRepository;
            // _tenantManager = tenantManager;
            // _tenantRepo = tenantRepo;
            // _session = session;
            
        }
        public async Task CreateAsync(BusinessDetailsInputDto input)
        {
            
            // var tenantDto = new CreateTenantDto{
            //     TenancyName = "tenant_" + input.Title,
            //     Name  = "tenant_" + input.Title,
            //     // AdminEmailAddress = input.
            // };
            // var tenant = ObjectMapper.Map<Tenant>(tenantDto);
            
            var @business = Business.Create(
                1,
                input.Title,
                input.WebURL,
                input.Description,
                input.RegNo,
                input.VATNo,
                input.BEEStatus,
                input.MonthEnd,
                input.PAYENo,
                input.UIFNo,
                input.Telephone,
                input.OwnerId
            );

            await _businessManager.CreateAsync(@business);
        }

        public async Task DeleteAsync(EntityDto<Guid> input)
        {
            var @business = await _businessManager.GetAsync(input.Id);
            await _businessManager.DeleteAsync(@business);
        }

        public async Task EditAddressAsync(BusinessAddressInputDto input)
        {
            var @business = await _businessRepository.GetAsync(input.Id);
            if (@business == null)
            {
                throw new UserFriendlyException("failed locating business.");
            }
            @business.EditAddress(input.AddressLine1,input.AddressLine2,input.Suburb,input.ZipCode);
            await _businessRepository.UpdateAsync(@business);
        }

        public async Task EditBankingAsync(BusinessBankingInputDto input)
        {
            var @business = await _businessRepository.GetAsync(input.Id);
            if (@business == null)
            {
                throw new UserFriendlyException("failed locating business.");
            }
            business.EditBankning(input.BankName,input.AccountNo,input.BranchCode,input.BranchName);
            await _businessRepository.UpdateAsync(@business);
        }

        public async Task<BusinessDetailOutputDto> GetDetailAsync(EntityDto<Guid> input)
        {
            var @business = await _businessRepository
                .GetAll()
                .Include(e => e.Employees)
                .Where(e => e.Id == input.Id)
                .FirstOrDefaultAsync();

            if (@business == null)
            {
                throw new UserFriendlyException("Could not locate business, maybe it's deleted.");
            }

            return @business.MapTo<BusinessDetailOutputDto>();
        }

        public async Task<ListResultDto<BusinessListDto>> GetListAsync(EntityDto<long> input)
        {
            var businesses = await _businessRepository
                .GetAll()
                .Include(e => e.Employees)
                .Where(e => e.OwnerId == input.Id)
                .OrderBy(e => e.Title)
                .Take(64)
                .ToListAsync();

            return new ListResultDto<BusinessListDto>(businesses.MapTo<List<BusinessListDto>>());
        }
        public async Task<ListResultDto<BusinessListDto>> GetUserListAsync()
        {
            if (!AbpSession.UserId.HasValue)
            {
                return new ListResultDto<BusinessListDto>();
            }
            var businesses = await _businessRepository
                .GetAll()
                .Include(e => e.Employees)
                .Where(e => e.OwnerId == AbpSession.UserId)
                .OrderBy(e => e.Title)
                .Take(64)
                .ToListAsync();

            return new ListResultDto<BusinessListDto>(businesses.MapTo<List<BusinessListDto>>());
        }

        public async Task UpdateAsync(BusinessInputDto input)
        {
            var @business = await _businessRepository.GetAsync(input.Id);
            if (@business == null)
            {
                throw new UserFriendlyException("failed locating business.");
            }
            @business.Edit(
                input.Title,
                input.WebURL,
                input.Description,
                input.RegNo,
                input.VATNo,
                input.BEEStatus,
                input.MonthEnd,
                input.RegNo,
                input.UIFNo,
                input.Telephone
                );
            await _businessRepository.UpdateAsync(@business);
        }
    }
}