using System;
using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;

namespace nettpay.Businesses.Dtos
{
    [AutoMapFrom(typeof(Business))]
    public class BusinessInputDto : EntityDto<Guid>
    {
        [Required]
        public string RegNo { get; set; }

        [Required]
        [StringLength(Business.MaxTitleLength)]
        public string Title { get; set; }

        [StringLength(Business.MaxDescriptionLength)]
        public string Description { get; set; }
        public byte MonthEnd { get; set; }
        public string UIFNo { get; set; }
        [Range(0, long.MaxValue)]
        public long OwnerId { get; set; }
        public string Telephone { get; set; }
        [StringLength(Business.MaxPhoneLength)]

        public string VATNo { get; set; }
        public string PAYENo { get; set; }
        public string WebURL { get; set; }
        public string BEEStatus { get; set; }
    }
}