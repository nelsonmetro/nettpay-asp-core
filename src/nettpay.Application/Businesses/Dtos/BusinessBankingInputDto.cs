using System;
using System.ComponentModel.DataAnnotations;

namespace nettpay.Businesses.Dtos
{
    public class BusinessBankingInputDto
    {
        [Required]
        public Guid Id { get; set; }
        [Required]
        public string AccountNo { get; set; }
        [Required]

        [StringLength(Business.MaxTitleLength)]
        public string BankName { get; set; }
        public string BranchCode { get; set; }

        [StringLength(Business.MaxTitleLength)]
        public string BranchName { get; set; }
    }
}