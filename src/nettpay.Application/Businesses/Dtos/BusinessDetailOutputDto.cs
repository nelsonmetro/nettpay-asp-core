using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using nettpay.Employees.Dtos;

namespace nettpay.Businesses.Dtos
{
    [AutoMapFrom(typeof(Business))]
    public class BusinessDetailOutputDto  : FullAuditedEntityDto<Guid>
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string Suburb { get; set; }
        public string ZipCode { get; set; }
        public string Telephone { get; set; }
        public string WebURL { get; set; }
        public byte MonthEnd { get; set; }
        public string PAYENo { get; set; }
        public string RegNo { get; set; }        
        public string UIFNo { get; set; }
        public string VATNo { get; set; }
        public string BEEStatus { get; set; }
        [StringLength(Business.MaxTitleLength)]
        public virtual string BankName { get; set; }
        public virtual string AccountNo { get; set; }
        [StringLength(Business.MaxTitleLength)]
        public virtual string BranchName { get; protected set; }
        public virtual string BranchCode { get; protected set; }
        public ICollection<EmployeeDto> Employees { get; set; }
    }
}