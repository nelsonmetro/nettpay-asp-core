using System;
using System.ComponentModel.DataAnnotations;

namespace nettpay.Businesses.Dtos
{
    public class BusinessAddressInputDto
    {
        [Required]
        public Guid Id { get; set; }
        [Required]
        [StringLength(Business.MaxAddressLength)]
        public string AddressLine1 { get; set; }

        [Required]
        [StringLength(Business.MaxAddressLength)]
        public string AddressLine2 { get; set; }

        [Required]
        [StringLength(Business.MaxAddressLength)]
        public string Suburb { get; set; }

        [Required]
        public string ZipCode { get; set; }
    }
}