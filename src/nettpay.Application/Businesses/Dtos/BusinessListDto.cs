using System;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;

namespace nettpay.Businesses.Dtos
{
    [AutoMapFrom(typeof(Business))]
    public class BusinessListDto :  FullAuditedEntityDto<Guid>
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public string Regno { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string Suburb { get; set; }
        public string Zip { get; set; }
        public string Telephone { get; set; }
        public string WebURL { get; set; }
        public byte MonthEnd { get; set; }
        public string PAYENo { get; set; }
        public string UIFNo { get; set; }
        public int EmployeesCount { get; set; }
    }
}