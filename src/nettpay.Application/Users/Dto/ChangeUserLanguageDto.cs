using System.ComponentModel.DataAnnotations;

namespace nettpay.Users.Dto
{
    public class ChangeUserLanguageDto
    {
        [Required]
        public string LanguageName { get; set; }
    }
}