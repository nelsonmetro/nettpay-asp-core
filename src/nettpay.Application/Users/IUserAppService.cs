using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using nettpay.Roles.Dto;
using nettpay.Users.Dto;

namespace nettpay.Users
{
    public interface IUserAppService : IAsyncCrudAppService<UserDto, long, PagedUserResultRequestDto, CreateUserDto, UserDto>
    {
        Task<ListResultDto<RoleDto>> GetRoles();

        Task ChangeLanguage(ChangeUserLanguageDto input);
    }
}
